<?php

namespace App\Http\Controllers;

use App\Messages;
use Illuminate\Http\Request;
use DB;

class MessagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        header("Access-Control-Allow-Origin: *");
        return '{"time":{"updated":"Mar 23, 2020 09:24:00 UTC","updatedISO":"2020-03-23T09:24:00+00:00","updateduk":"Mar 23, 2020 at 09:24 GMT"},"disclaimer":"This data was produced from the CoinDesk Bitcoin Price Index (USD). Non-USD currency data converted using hourly conversion rate from openexchangerates.org","chartName":"Bitcoin","bpi":{"USD":{"code":"USD","symbol":"&#36;","rate":"5,769.5800","description":"United States Dollar","rate_float":5769.58},"GBP":{"code":"GBP","symbol":"&pound;","rate":"4,961.9369","description":"British Pound Sterling","rate_float":4961.9369},"EUR":{"code":"EUR","symbol":"&euro;","rate":"5,388.1819","description":"Euro","rate_float":5388.1819}}}';
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        header("Access-Control-Allow-Origin: *");
        return 'create';
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        header("Access-Control-Allow-Origin: *");
        return 'store';
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Messages  $messages
     * @param from route we get a variable message
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Messages $messages, $message)
    {
        header("Access-Control-Allow-Origin: *");
        $data = $request->input('sentMessage');
        return $message.' show '.$data;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Messages  $messages
     * @return \Illuminate\Http\Response
     */
    public function edit(Messages $messages)
    {
        header("Access-Control-Allow-Origin: *");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Messages  $messages
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Messages $messages)
    {
        header("Access-Control-Allow-Origin: *");
        return 'update';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Messages  $messages
     * @return \Illuminate\Http\Response
     */
    public function destroy(Messages $messages)
    {
        header("Access-Control-Allow-Origin: *");
    }
}

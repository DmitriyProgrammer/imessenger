<?php

namespace App\Classes\Socket;

use App\Classes\Socket\Base\BaseSocket;
use Ratchet\ConnectionInterface;
use Ratchet\MessageComponentInterface;

class ChatSocket extends BaseSocket {

    protected $clients;

    public function __construct() {
        $this->clients  = new \SplObjectStorage;
    }

    public function onOpen(ConnectionInterface $conn) {
        $this->clients->attach($conn);
    }

    public function onMessage(ConnectionInterface $from, $msg) {
        echo('Connection '. $from->resourceId.' Send message '.$msg." \r\n");
        foreach ($this->clients as $client)
        {
            // if($from != $client) {
                $client->send($msg);
            // }
        }
    }

    public function onClose(ConnectionInterface $conn) {
        $this->clients->detach($conn);
        echo('Connection '.$conn->resourceId." was closed \r\n");
    }

    public function onError(ConnectionInterface $conn, \Exception $e) {
        echo('We have a problem '.$e->getMessage()." \r\n");
        $conn->close();
    }
}

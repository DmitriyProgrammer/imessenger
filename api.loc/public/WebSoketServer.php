<?php
// For fork Websoket, write command in command line: 'php imessenger.loc/api.loc/public/WebSoketServer.php start'
use Workerman\Worker;

require_once 'C:/OS/OSPanel/domains/imessenger.loc/api.loc/vendor/autoload.php';


// Create a Websocket server
$ws_worker = new Worker('websocket://127.0.0.1:59723');
$ws_worker->count = 4;

// Emitted when new connection come
$ws_worker->onConnect = function ($connection) {
    // echo "New connection\n";
    $connection->send('Hello from server!!!');

    // \Workerman\Lib\Timer::add(1, function () use ($connection) {
    //     $connection->send('Hello from server!!!');
    // } );
};

// Emitted when data received
$ws_worker->onMessage = function ($connection, $data) {
    // Send hello $data
    $connection->send('I got message from Frontend: ' . $data);
};

// Emitted when connection closed
$ws_worker->onClose = function ($connection) {
    echo "Connection closed\n";
};

// Run worker
Worker::runAll();
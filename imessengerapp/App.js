import React, { Component } from 'react';
import { ScrollView, Button, StyleSheet, Text, TextInput, View } from 'react-native';

export default class PizzaTranslator extends Component {
  constructor(props) {
    super(props);
    this.state = {
      textInput: '',
      textOutput: ''
    };
  }
  _onPressButton(tmp) {
    this.setState({  
      textOutput: tmp 
    });
  }
  
  render() {
    return (
      <ScrollView>
        <View style={styles.container}>
          <Text style={styles.textFieldView}>
            {this.state.textOutput}
          </Text>
          <View style={styles.alternativeLayoutButtonContainer}>
            <TextInput
              style={styles.textInputSend}
                placeholder=" Сообщение"
                onChangeText={(textInput) => this.setState({textInput})}
              value={this.state.textInput}
             />
            <Button
              onPress={(textInput) => this._onPressButton(this.state.textInput)}
              title="Отправить"
            />
          </View>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 22
   },
  alternativeLayoutButtonContainer: {
    margin: 20,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  textFieldView: {
    padding: 10, 
    fontSize: 42,
    borderWidth: 1,
    borderRadius:10,
    borderColor: '#808080',
    margin: 10
  },
  buttonSend: {
    padding: 10
  },
  textInputSend: {
    height: 40,
    borderWidth: 1,
    borderRadius:10,
    borderColor: '#808080',
    width: '60%'
  }
});
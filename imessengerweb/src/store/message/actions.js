export const select = (message) => {
    return {
        type: "MESSAGE_SELECTED",
        payload: message
    }
};
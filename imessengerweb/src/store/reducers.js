import { combineReducers } from "redux";
import { authReducer } from './auth/reducers';
import { registrationReducer } from './registration/reducers';
import MessageReducers from './message/reducers';
import ActiveMessage from './active/reducers';

export default combineReducers({
    auth: authReducer,
    registration: registrationReducer,
    messages: MessageReducers,
    active: ActiveMessage
});
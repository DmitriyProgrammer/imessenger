import React from 'react';
import './App.css';
import {BrowserRouter, Route} from 'react-router-dom';
import WindowMessage from './components/WindowMessage';
import SendButton from './components/SendButton';
import SendTextArea from './components/SendTextArea';
import TextMessage from './components/TextMessage';
import FormMessage from './components/FormMessage';

function App() {

    return (
        <BrowserRouter>
            <div>
                <FormMessage />
                {/* <TextMessage />
                <SendTextArea />
                <SendButton />
                <Route path='/catalog' component={WindowMessage} /> */}
            </div>
        </BrowserRouter>
    );
}

export default App;
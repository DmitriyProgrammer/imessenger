import React from 'react';

export default function SendButton() {
    const styles = {
        btn: {
            margin: 10
        }
    }
    return (
        <div>
            <input type="button" value="Send" style={styles.btn}></input>
        </div>
    );
}
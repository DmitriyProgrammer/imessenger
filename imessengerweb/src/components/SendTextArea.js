import React from 'react';

export default function SendTextArea() {
    const styles = {
        txtar: {
            marginLeft: '1em',
            width: '95vw'
        }
    }
    return (
        <div>
            <textarea style={styles.txtar}></textarea>
        </div>
    )
}
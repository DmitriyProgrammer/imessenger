import React from 'react';
const axios = require('axios');

export default class FormMessage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {value: '', message: '', tmp: 1, winMess: '', allMessages: []};
    
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
  
    handleChange(event) {
        this.setState({value: event.target.value});
    }

    handleSubmit(event) {

        this.state.allMessages.push(this.state.value);
        // console.info(this.state.allMessages);

        this.setState({message: this.state.value});   

        axios.get('http://imessenger.loc/api.loc/public/api/message/11?sentMessage='+this.state.allMessages)
            .then(response => (console.info(response)))
            .catch(error => console.log(error));

        event.preventDefault();
    }
  
    componentDidMount() {
        // console.info('am ready');
    }

    componentDidUpdate() {
        // console.info('am update');
        // this.state.tmp++;
    }

    onDecrease() {}

    render() {

        let ws = new WebSocket('ws://127.0.0.1:59723');

        ws.addEventListener('message', (event) => {
            console.info('Message from Server: ' + event.data); // get from server
        })

        const func = () => {
            ws.send('Hello from Martians'); //send on server
        };
        setTimeout(func, 1 * 1000);

        const listItems = this.state.allMessages.map((number) =>
            <div key={number.toString()}>
                {number}
            </div>
        );

        return (
            <form onSubmit={this.handleSubmit}>
                <pre>
                    {listItems}
                </pre>
                <label>
                    Name:
                    <input type="text" value={this.state.value} onChange={this.handleChange} />
                </label>
                <input type="submit" value="Submit" onClick={this.onDecrease} />
            </form>
        );
    }
}
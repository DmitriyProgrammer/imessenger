import React from 'react';

export default class TextMessage extends React.Component{
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <pre style={styles.txtinf}>
                    text
                </pre>
            </div>
        )
    }
}

const styles = {
    txtinf: {
        marginLeft: '1em',
        width: '95vw'
    }
}
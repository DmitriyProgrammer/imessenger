import React from 'react';
import Details from '../containers/DetailsContainer';

export default function WindowMessage() {
    const styles = {
        pre: {
            padding: 10,
            margin: 10,
            border: 10,
            width: '100vw'
        }
      };
    return (
        <div>
            <pre style={styles.pre}>
                <Details />
            </pre>
        </div>
    );
}
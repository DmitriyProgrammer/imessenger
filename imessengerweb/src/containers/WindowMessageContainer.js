import React, {Component} from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { select } from '../store/message/actions';

class MessagesList extends Component {
    showList() {
        return this.props.messages.map((message) => {
            return (
                <li onClick={() => this.props.select(message) } 
                key={message.id}>{message.color}</li>
            );
        });
    }
    render() {
        return(
            <div>
                <ol>
                    {this.showList()}
                </ol>
                <div>{this.props.auth.email}</div>
            </div>
                
        )
    }
}

function mapStateToProps(state) {
    return {
        messages: state.messages,
        auth: state.auth 
    };
}

function matchDispatchToProps (dispatch) {
    return bindActionCreators({select: select}, dispatch)
}

export default connect(mapStateToProps, matchDispatchToProps)(MessagesList);
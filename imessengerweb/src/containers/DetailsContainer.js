import React, {Component} from 'react';
import { connect } from 'react-redux';

class Details extends Component {
    render () {
        if(!this.props.message) {
            return (<p>Change message</p>);
        }
        return (
            <div>
                <h2>{this.props.message.name}</h2>
                <p>{this.props.message.color}</p>
            </div>
        );
    }
}

function mapStateToProps (state) {
    return {
        message: state.active
    }
}

export default connect(mapStateToProps)(Details)